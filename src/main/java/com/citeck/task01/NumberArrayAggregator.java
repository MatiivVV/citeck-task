package com.citeck.task01;

import java.util.*;

import static java.lang.Integer.max;
import static java.util.Comparator.reverseOrder;

public class NumberArrayAggregator<T extends Number & Comparable> {

    /**
     * Для первичной агрегации и сортировки по значению используется красно-черное дерево.
     */
    public List<Aggregate<T>> aggregate(T[] array) {
        Map<T, Integer> aggregator = new TreeMap<>(reverseOrder());
        int maxCount = 0;
        for (T number : array) {
            Integer count = aggregator.get(number);
            if (count == null) {
                aggregator.put(number, 1);
                maxCount = max(maxCount, 1);
            } else {
                aggregator.put(number, count + 1);
                maxCount = max(maxCount, count + 1);
            }
        }
        return sortByCount(aggregator, maxCount);
    }

    /**
     * Для последующей сортировки по числу вхождений используется алгоритм counting sort т.к.:
     * - число вхождений может служить индексом массива;
     * - сортировка стабильна;
     * - сортировка выполняется за O(n+k).
     */
    private List<Aggregate<T>> sortByCount(Map<T, Integer> aggregator, int maxCount) {
        var countArray = new int[maxCount];
        aggregator.forEach((number, count) -> countArray[count - 1]++);
        for (int i = 1; i < countArray.length; i++) {
            countArray[i] += countArray[i - 1];
        }
        @SuppressWarnings("unchecked")
        var result = (Aggregate<T>[]) new Aggregate<?>[aggregator.size()];
        aggregator.forEach((number, count) -> {
            result[countArray[count - 1] - 1] = new Aggregate<>(number, count);
            countArray[count - 1]--;
        });
        return Arrays.asList(result);
    }

    public static class Aggregate<E> {

        private final E value;
        private final int count;

        public Aggregate(E value, int count) {
            this.value = value;
            this.count = count;
        }

        public E getValue() {
            return value;
        }

        public int getCount() {
            return count;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Aggregate<?> aggregate = (Aggregate<?>) o;
            return count == aggregate.count &&
                    value.equals(aggregate.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value, count);
        }

        @Override
        public String toString() {
            return "Aggregate{" +
                    "value=" + value +
                    ", count=" + count +
                    '}';
        }

    }

}
