package com.citeck.task02;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.citeck.task02.ExpressionValidator.Sequence.BRACKETS;
import static com.citeck.task02.ExpressionValidator.Sequence.PARENTHESES;
import static com.citeck.task02.ExpressionValidator.Symbol.forChar;
import static com.citeck.task02.ExpressionValidator.SymbolType.CLOSING;
import static com.citeck.task02.ExpressionValidator.SymbolType.OPENING;
import static java.util.Arrays.stream;

public class ExpressionValidator {

    /**
     * Для валидации используется стек открывающих символов: при прохождении открывающего символа
     * он добавляется в стек, а при прохождении закрывающего удаляется последний открывающий.
     * Кроме того, предусмотрена предварительная проверка на четное кол-во символов в выражении.
     */
    public boolean validate(CharSequence expression) {
        if (expression.length() / 2 == 1) {
            return false;
        }
        Deque<Sequence> sequenceStack = new LinkedList<>();
        for (int i = 0; i < expression.length(); i++) {
            Symbol symbol = forChar(expression.charAt(i));
            if (symbol == null) {
                return false;
            }
            switch (symbol.getType()) {
                case OPENING:
                    sequenceStack.push(symbol.getSequence());
                    break;
                case CLOSING:
                    if (sequenceStack.isEmpty() || sequenceStack.pop() != symbol.getSequence()) {
                        return false;
                    }
                    break;
                default:
                    return false;
            }
        }
        return sequenceStack.isEmpty();
    }

    public enum Symbol {

        OPENING_BRACKET('[', OPENING, BRACKETS),
        CLOSING_BRACKET(']', CLOSING, BRACKETS),
        OPENING_PARENTHESES('(', OPENING, PARENTHESES),
        CLOSING_PARENTHESES(')', CLOSING, PARENTHESES);

        private static Map<Character, Symbol> symbols = new HashMap<>();

        static {
            stream(values()).forEach(symbol -> symbols.put(symbol.getCharacter(), symbol));
        }

        private char character;
        private SymbolType type;
        private Sequence sequence;

        Symbol(char character, SymbolType type, Sequence sequence) {
            this.character = character;
            this.type = type;
            this.sequence = sequence;
        }

        public static Symbol forChar(char character) {
            return symbols.get(character);
        }

        public char getCharacter() {
            return character;
        }

        public SymbolType getType() {
            return type;
        }

        public Sequence getSequence() {
            return sequence;
        }

    }

    public enum SymbolType {

        OPENING,
        CLOSING

    }

    public enum Sequence {

        BRACKETS,
        PARENTHESES

    }

}
