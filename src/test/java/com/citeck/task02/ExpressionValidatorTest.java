package com.citeck.task02;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExpressionValidatorTest {

    private ExpressionValidator expressionValidator = new ExpressionValidator();

    @Test
    void testEmpty() {
        assertTrue(expressionValidator.validate(""));
    }

    @Test
    void testIllegalSymbol() {
        assertFalse(expressionValidator.validate("([][]([).])"));
    }

    @Test
    void testInvalid() {
        assertFalse(expressionValidator.validate("([][]([)])"));
    }

    @Test
    void testValid() {
        assertTrue(expressionValidator.validate("([][[]()])"));
    }

}
