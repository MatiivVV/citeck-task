package com.citeck.task01;

import org.junit.jupiter.api.Test;

import java.util.List;

import static com.citeck.task01.NumberArrayAggregator.Aggregate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NumberArrayAggregatorTest {

    private NumberArrayAggregator<Integer> aggregator = new NumberArrayAggregator<>();

    @Test
    void testEmpty() {
        Integer[] array = {};
        assertTrue(aggregator.aggregate(array).isEmpty());
    }

    @Test
    void testSingleValue() {
        Integer[] array = {23};
        List<Aggregate<Integer>> result = aggregator.aggregate(array);
        assertEquals(1, result.size());
        assertEquals(new Aggregate<>(23, 1), result.get(0));
    }

    @Test
    void testMultipleValues() {
        Integer[] array = {2, 1, 3, 5, 3, 6, 3, 1, 2, 4};
        List<Aggregate<Integer>> expected = List.of(
                new Aggregate<>(4, 1),
                new Aggregate<>(5, 1),
                new Aggregate<>(6, 1),
                new Aggregate<>(1, 2),
                new Aggregate<>(2, 2),
                new Aggregate<>(3, 3)
        );
        List<Aggregate<Integer>> result = aggregator.aggregate(array);
        assertEquals(6, result.size());
        assertEquals(expected, result);
    }

}
