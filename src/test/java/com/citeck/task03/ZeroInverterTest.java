package com.citeck.task03;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZeroInverterTest {

    private ZeroInverter zeroInverter = new ZeroInverter();

    @Test
    void testMinValue() {
        assertEquals(~0, zeroInverter.invert(~0));
    }

    @Test
    void test() {
        assertEquals(0b101101111, zeroInverter.invert(0b101100111));
    }

}
